import firebase from "firebase";

const config = {
  apiKey: "AIzaSyChl-BJheNw-THdIFFK-tTU_7P3omX9LWs",
  authDomain: "flowius-admin.firebaseapp.com",
  databaseURL: "https://flowius-admin.firebaseio.com",
  projectId: "flowius-admin",
  storageBucket: "flowius-admin.appspot.com",
  messagingSenderId: "977586871668"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export default firebase;
export const auth = firebase.auth();
export const db = firebase.firestore();
