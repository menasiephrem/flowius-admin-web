import { db } from "./firebase_init";

export default class API {
  static getManager(manger) {
    return new Promise((resolve, reject) => {
      db.collection("Employees")
        .where("email", "==", manger)
        .get()
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }
}
