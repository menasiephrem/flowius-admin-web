import Cookies from "universal-cookie";

class ClientSession {
  static authkey = "auth";
  static loggedin = null;
  static cookies = new Cookies();

  static storeAuth = value => {
    localStorage.setItem(ClientSession.authkey, JSON.stringify(value));
  };

  static getAuth = () => {
    return JSON.parse(localStorage.getItem(ClientSession.authkey));
  };

  static removeAuth = () => {
    localStorage.removeItem(ClientSession.authkey);
  };

  static isLoggedIn = () => {
    return localStorage.getItem(ClientSession.authkey);
  };

  static Logout = () => {
    return localStorage.removeItem(ClientSession.authkey);
  };

  static getToken = () => {
    if (ClientSession.isLoggedIn()) {
      const user = ClientSession.getAuth();
      return user.credential.idToken;
    }
  };

  static getAccessToken = () => {
    if (ClientSession.isLoggedIn()) {
      return ClientSession.getAuth();
    }
  };
}

export default ClientSession;
