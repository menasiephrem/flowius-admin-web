import { db } from "./firebase_init";
export default class Fund {
  static model(snapshot) {
    const funds = [];
    snapshot.forEach(doc => {
      const fund = doc.data();
      funds.push(Fund.getFundModel(fund, doc.id));
    });
    return funds;
  }

  static getFund(creater) {
    if (creater === "chris@3blenterprises.com") {
      return new Promise((resolve, reject) => {
        db.collection("Funds")
          .get()
          .then(data => resolve(data))
          .catch(err => reject(err));
      });
    } else {
      return new Promise((resolve, reject) => {
        db.collection("Funds")
          .where("createdBy", "==", creater)
          .get()
          .then(data => resolve(data))
          .catch(err => reject(err));
      });
    }
  }

  static createFund(fund) {
    return new Promise((resolve, reject) => {
      db.collection("Funds")
        .doc(fund.fundId)
        .set(fund)
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }

  static updateFund(id, value) {
    return new Promise((resolve, reject) => {
      db.collection("Funds")
        .doc(id)
        .update(value)
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }
  static getFundsInTime(startDate, endDate) {
    return new Promise((resolve, reject) => {
      db.collection("Funds")
        .where("createdAt", ">=", startDate)
        .where("createdAt", "<=", endDate)
        .get()
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }
  static getFundModel(fund, _id) {
    const date = fund.createdAt.toDate();
    let totalPrice = 0;
    fund.items.map(item => {
      return (totalPrice += item.amount);
    });
    let ret = {};
    ret._id = _id;
    ret.fundId = fund.fundId;
    ret.fundRequester = fund.craterName;
    ret.reviewed = fund.reviewed;
    ret.approved = fund.approved;
    ret.fundItems = `${fund.items.length} ${
      fund.items.length === 1 ? "Item" : "Items"
    }`;
    ret.fundDate = `${date.toLocaleString("en-us", {
      month: "long"
    })} ${date.getDate()}, ${date.getFullYear()}`;
    ret.fundAmount = `${totalPrice} Birr`;
    ret.checkNumber = fund.checkNumber;
    return ret;
  }
}
