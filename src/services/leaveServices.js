import { db } from "./firebase_init";
import _ from "lodash";
import ClientSession from "./client-session";
export default class Leave {
  static model(snapshot) {
    const leaves = [];
    snapshot.forEach(doc => {
      const fund = doc.data();
      leaves.push(Leave.getLeaveModel(fund, doc.id));
    });
    return leaves;
  }

  static getLeave(creater) {
    if (creater === "chris@3blenterprises.com") {
      return new Promise((resolve, reject) => {
        db.collection("Leaves")
          .get()
          .then(data => resolve(data))
          .catch(err => reject(err));
      });
    } else {
      return new Promise((resolve, reject) => {
        db.collection("Leaves")
          .where("email", "==", creater)
          .get()
          .then(data => resolve(data))
          .catch(err => reject(err));
      });
    }
  }

  static createLeave(leave) {
    return new Promise((resolve, reject) => {
      db.collection("Leaves")
        .doc(leave.id)
        .set(leave)
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }

  static prepareLeave(firstDate, endDate, type) {
    let leave = {};
    let now = new Date();

    leave.approved = false;
    leave.createdAt = now;
    leave.updatedAt = now;
    leave.description = type;
    leave.duration = `${firstDate.format("MMMM Do")} To ${endDate.format(
      "MMMM Do"
    )}`;
    leave.email = ClientSession.getAuth().email;
    leave.from = firstDate.toDate();
    leave.id = `${now.getFullYear()}${("0" + (now.getMonth() + 1)).slice(
      -2
    )}.${Math.floor(1000 + Math.random() * 9000)}`;
    leave.manager = "chris@3blenterprises.com";
    leave.name = ClientSession.getAuth().username;
    leave.requestedAmount = Leave.getDate(firstDate.clone(), endDate.clone());
    leave.reviewed = false;
    leave.to = endDate.toDate();
    leave.type = type.charAt(0).toUpperCase() + _.camelCase(type).slice(1);
    return leave;
  }

  static getLeaveModel(leave, _id) {
    const opt = {
      weekday: "short",
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric"
    };
    const date = leave.createdAt.toDate();
    let ret = {};
    ret._id = _id;
    ret.leaveId = leave.id;
    ret.leaveRequester = leave.name;
    ret.approved = leave.approved;
    ret.createdAt = `${date.toLocaleString("en-us", {
      month: "long"
    })} ${date.getDate()}, ${date.getFullYear()}`;
    ret.duration = leave.duration;
    ret.type = leave.type.replace(/([a-z])([A-Z])/g, "$1 $2");
    ret.requestedAmount = `${leave.requestedAmount} Hours`;
    ret.from = leave.from.toDate().toLocaleDateString("en-US", opt);
    ret.to = leave.to.toDate().toLocaleDateString("en-US", opt);
    ret.reviewed = leave.reviewed;
    return ret;
  }

  static getNewLeave(leave) {
    const opt = {
      weekday: "short",
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric"
    };
    const date = leave.createdAt;
    let ret = {};
    ret._id = leave.id;
    ret.leaveId = leave.id;
    ret.leaveRequester = leave.name;
    ret.approved = leave.approved;
    ret.createdAt = `${date.toLocaleString("en-us", {
      month: "long"
    })} ${date}, ${date.getFullYear()}`;
    ret.duration = leave.duration;
    ret.type = leave.type.replace(/([a-z])([A-Z])/g, "$1 $2");
    ret.requestedAmount = `${leave.requestedAmount} Hours`;
    ret.from = leave.from.toLocaleDateString("en-US", opt);
    ret.to = leave.to.toLocaleDateString("en-US", opt);
    return ret;
  }

  static getLeaveStatusModel(doc) {
    const data = doc.data();
    let ret = {};
    ret["Sick Leave"] = data.sickLeaveWithOutDoctorNote;
    ret["Annual Leave"] = data.annualLeave;
    ret[`Sick Leave With Doctors Note`] = data.sickLeave;
    return ret;
  }

  static getLeaveStatus(email) {
    return new Promise((resolve, reject) => {
      db.doc(`Employees/${email}`)
        .get()
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }

  static updateLeave(id, value) {
    return new Promise((resolve, reject) => {
      db.collection("Leaves")
        .doc(id)
        .update(value)
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }

  static getDate = (first, secound) => {
    let TotalHour = 0;

    if (first.day() !== 0 && first.day() !== 6) {
      if (first.hour() <= 9) TotalHour += 8;
      else if (first.hour() >= 9 && first.hour() < 17)
        TotalHour += 17 - first.hour();
    }

    if (first.dayOfYear() === secound.dayOfYear()) return TotalHour;

    first.add(1, "days");

    while (first.dayOfYear() !== secound.dayOfYear()) {
      if (first.day() !== 0 && first.day() !== 6) TotalHour += 8;
      first.add(1, "days");
    }

    if (secound.day() !== 0 && secound.day() !== 6) {
      if (first.hour() <= 9) TotalHour += 8;
      else if (secound.hour() >= 9 && secound.hour() < 17)
        TotalHour += secound.hour() - 9;
    }

    return TotalHour;
  };
}
