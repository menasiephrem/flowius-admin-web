import Raven from "raven-js";

function init() {
  Raven.config("https://c3a27d206b0c45578a8520766093af9f@sentry.io/1428910", {
    release: "1-0-0",
    environment: "development-test"
  }).install();
}

function log(error) {
  Raven.captureException(error);
}

export default {
  init,
  log
};
