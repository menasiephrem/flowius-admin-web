import { db } from "./firebase_init";

export default class Employee {
  static model(snapshot) {
    const employees = [];
    snapshot.forEach(doc => {
      const employee = doc.data();
      employees.push(Employee.getModel(employee));
    });
    return employees;
  }

  static getEmployees() {
    return new Promise((resolve, reject) => {
      db.collection("Employees")
        .get()
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
  }

  static getModel(data) {
    const employee = {};
    employee.email = data.email;
    employee.name = data.name;
    employee.picUrl = data.picUrl;
    employee.sickLeaveWDoctor = `${data.sickLeave} Hours`;
    employee.sickLeave = `${data.sickLeaveWithOutDoctorNote} Hours`;
    employee.annualLeave = `${data.annualLeave} Hours`;
    return employee;
  }
}
