import firebase from "firebase";
import API from "./API";
import ClientSession from "./client-session";
const provider = new firebase.auth.GoogleAuthProvider();

export const doSignUpWithGoogle = () => {
  return firebase
    .auth()
    .signInWithPopup(provider)
    .then(async result => {
      const email = result.user.email;
      let emp = await API.getManager(email);
      if (emp.size > 0) {
        const user = {
          userId: email,
          username: result.user.displayName,
          email: result.user.email,
          photoURL: result.user.photoURL,
          credential: result.credential,
          isAdmin:
            email === "menasi@3blenterprises.com" ||
            email === "chris@3blenterprises.com"
        };
        ClientSession.storeAuth(user);
        return {
          success: true,
          message: "Login Success"
        };
      } else {
        return {
          error: true,
          message: "you are not allowed"
        };
      }
    })
    .catch(error => {
      console.log(error);
      return {
        error: true,
        message: error.message
      };
    });
};
