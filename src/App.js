import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import LoginForm from "./components/login";
import Dashbord from "./components/dashbord";
import ProtectedRoute from "./components/common/protectedRoute";
import PageNotFound from "./components/common/pageNotFound";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import Logout from "./components/logout";
import ClientSession from "./services/client-session";

class App extends Component {
  render() {
    const logedIn = ClientSession.isLoggedIn();
    return (
      <div>
        <ToastContainer />
        <Switch>
          <Route path="/login" component={LoginForm} />
          <Route path="/logout" component={Logout} />
          <ProtectedRoute
            protection={logedIn}
            path="/dashbord"
            component={Dashbord}
          />
          <Route path="/not-found" component={PageNotFound} />
          <Redirect from="/" exact to="/dashbord/funds" />
          <Redirect to="/not-found" />
        </Switch>
      </div>
    );
  }
}

export default App;
