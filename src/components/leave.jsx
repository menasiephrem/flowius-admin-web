import React, { Component } from "react";
import _ from "lodash";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { FirestoreDocument } from "react-firestore";
import Pie from "./common/pieChart";
import ListGroup from "./common/listGroup";
import { toast } from "react-toastify";
import Loading from "./common/loading";
import Search from "./common/search";
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";
import LeaveTable from "./leaveTable";
import LeaveAPI from "../services/leaveServices";
import ClientSession from "../services/client-session";
import SingleLeave from "./singleLeave";
import DatePicker from "./common/datePickerModal";
import "./stylesheet.css";
//This is to restart 1
class Leave extends Component {
  constructor(props) {
    super(props);
    this.handleDateRange = this.handleDateRange.bind(this);
  }
  state = {
    leaveLeft: [
      {
        value: 72,
        color: "#E38627"
      }
    ],
    currentSelction: "Sick Leave",
    catagories: [
      { name: "Sick Leave", _id: "SickLeave" },
      { name: "Annual Leave", _id: "AnnualLeave" },
      { name: "Sick Leave With Doctors Note", _id: "SickLeaveWithDoctorNote" }
    ],
    leaveStatus: {
      "Sick Leave": 48,
      "Annual Leave": 128,
      "Sick Leave With Doctors Note": 520
    },
    totalLeavs: {
      "Sick Leave": 48,
      "Annual Leave": 128,
      "Sick Leave With Doctors Note": 520
    },
    isLoading: true,
    sortColumn: { path: "leaveId", order: "asc" },
    searchPhrase: "",
    pageSize: 8,
    currentPage: 1,
    currentTotalAmount: 128,
    leaves: [],
    currentLeave: "",
    modalOpen: false,
    newLeaveOpen: false,
    startDate: null,
    finishDate: null,
    dateNumber: 0,
    errorOnNewFund: false
  };

  style = {
    width: "60%",
    textAlign: "center"
  };

  handleSearch = query => {
    this.setState({
      searchPhrase: query,
      currentPage: 1,
      currentSelction: null
    });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handlePageChange = currentPage => {
    this.setState({ currentPage });
  };

  handleReviewChange = currentSelction => {
    this.setState({ currentSelction });
  };

  handleDetail = id => {
    this.setState({ currentLeave: id, modalOpen: true });
  };

  handleToggle = () => {
    this.setState({ modalOpen: false });
  };

  toggelNewFund = () => {
    this.setState({ newLeaveOpen: !this.state.newLeaveOpen });
  };

  handleSave = async () => {
    const { startDate, finishDate, currentSelction } = this.state;
    if (startDate && finishDate) {
      toast("Creating Leave ...");
      const leave = LeaveAPI.prepareLeave(
        startDate,
        finishDate,
        currentSelction
      );
      await LeaveAPI.createLeave(leave);
      toast("Leave Created");
      const { leaveStatus: ls } = this.state;
      const leaves = [...this.state.leaves];
      const leaveStatus = JSON.parse(JSON.stringify(ls));
      leaves.push(LeaveAPI.getNewLeave(leave));
      leaveStatus[currentSelction] =
        leaveStatus[currentSelction] - leave.requestedAmount;
      this.setState({
        errorOnNewFund: false,
        newLeaveOpen: false,
        leaves,
        leaveStatus
      });
    } else {
      toast.error("Please Select Start and Finsh Date");
    }
  };

  handleApprove = async (id, status) => {
    toast("Updating Leave");
    await LeaveAPI.updateLeave(id, { approved: status, reviewed: true });
    const leaves = [...this.state.leaves];
    const index = leaves.findIndex(i => (i._id = id));
    leaves[index].approved = status;
    leaves[index].reviewed = true;
    this.setState({ leaves, modalOpen: false });
  };

  handleDateRange(startDate, endDate) {
    const dateNumber = LeaveAPI.getDate(startDate.clone(), endDate.clone());

    this.setState({
      startDate: startDate,
      finishDate: endDate,
      dateNumber
    });
  }

  async componentDidMount() {
    const leaves = LeaveAPI.model(
      await LeaveAPI.getLeave(ClientSession.getAuth().email)
    );
    this.setState({ leaves, isLoading: false });
    const leaveStatus = LeaveAPI.getLeaveStatusModel(
      await LeaveAPI.getLeaveStatus(ClientSession.getAuth().email)
    );
    this.setState({ leaveStatus });
  }

  getpagedData = () => {
    const {
      pageSize,
      currentPage,
      leaves,
      currentSelction,
      sortColumn,
      searchPhrase
    } = this.state;

    let filterdLeaves = [...leaves];
    if (searchPhrase !== "")
      filterdLeaves = leaves.filter(f =>
        f.leaveId.toLowerCase().startsWith(searchPhrase.toLowerCase())
      );
    else filterdLeaves = leaves.filter(leave => currentSelction === leave.type);

    const sort = _.orderBy(
      filterdLeaves,
      [sortColumn.path],
      [sortColumn.order]
    );
    const PagnatedFunds = paginate(sort, currentPage, pageSize);
    const count = filterdLeaves.length;

    return { count, PagnatedFunds };
  };

  render() {
    const {
      catagories,
      currentSelction,
      isLoading,
      searchPhrase,
      sortColumn,
      pageSize,
      currentPage,
      totalLeavs,
      leaveStatus,
      modalOpen,
      currentLeave,
      newLeaveOpen,
      startDate,
      finishDate,
      dateNumber
    } = this.state;
    const { count, PagnatedFunds: leaves } = this.getpagedData();
    const data = [];
    let d = { color: "#E38627" };
    d.value = leaveStatus[currentSelction];
    const total = totalLeavs[currentSelction];
    data.push(d);

    const isAdmin = ClientSession.getAuth().isAdmin;

    return (
      <React.Fragment>
        {modalOpen && (
          <FirestoreDocument
            path={`Leaves/${currentLeave}`}
            render={({ isLoading, data }) => {
              return isLoading ? (
                <Loading modal />
              ) : (
                <Modal isOpen={true}>
                  <ModalHeader>
                    {LeaveAPI.getLeaveModel(data).leaveId}
                  </ModalHeader>
                  <ModalBody>
                    <SingleLeave
                      Items={data.items}
                      Leave={LeaveAPI.getLeaveModel(data)}
                    />
                  </ModalBody>
                  <ModalFooter>
                    {isAdmin && !data.reviewed && (
                      <button
                        color="primary"
                        className="btn btn-danger mr-auto"
                        onClick={() => this.handleApprove(currentLeave, false)}
                      >
                        Decline Request
                      </button>
                    )}
                    {isAdmin && !data.reviewed && (
                      <Button
                        color="primary"
                        onClick={() => this.handleApprove(currentLeave, true)}
                      >
                        Apporve Request
                      </Button>
                    )}
                    <Button color="secondary" onClick={this.handleToggle}>
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              );
            }}
          />
        )}
        <DatePicker
          isOpen={newLeaveOpen}
          headerText={`New ${currentSelction}`}
          startDate={startDate}
          finishDate={finishDate}
          handleDateRange={this.handleDateRange}
          handleSave={this.handleSave}
          toggelCancel={this.toggelNewFund}
          dateNumber={dateNumber}
        />
        <div className="row">
          <div className="col-3 m-2">
            <ListGroup
              categories={catagories}
              selectedItem={currentSelction}
              onItemSelect={this.handleReviewChange}
            />
          </div>
          <div className="col-1" />
          <div className="col-1" />
          <div className="col-3 ">
            <div style={this.style}>
              <Pie data={data} totalValue={total} />
            </div>
            <p className="p-1">{currentSelction} Left in hrs</p>
          </div>
        </div>
        <div className="row">
          <div className="col-3" />
          <div className="col">
            <button
              onClick={this.toggelNewFund}
              className="btn btn-primary m-2"
            >
              New Leave
            </button>
            <p className="mt-2">{`Showing ${count} Leave Requets in the databse`}</p>

            {isLoading ? (
              <Loading />
            ) : (
              <div>
                <Search value={searchPhrase} onChange={this.handleSearch} />
                <LeaveTable
                  onClick={this.handleDetail}
                  leaves={leaves}
                  sortColumn={sortColumn}
                  onSort={this.handleSort}
                />

                <Pagination
                  itemsCount={count}
                  pageSize={pageSize}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Leave;
