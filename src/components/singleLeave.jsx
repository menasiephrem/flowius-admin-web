import React from "react";
import approved from "../assets/approved.png";
import rejected from "../assets/rejected.png";
import pending from "../assets/pending.png";
import "./stylesheet.css";

const SingleLeave = ({ Leave }) => {
  const img = Leave.reviewed ? (Leave.approved ? approved : rejected) : pending;
  const style = { color: "#7b7b7b" };
  return (
    <div className="text-center font-weight-light">
      <h2>{Leave.leaveRequester}</h2>
      <h2>{Leave.requestedAmount}</h2>
      <h5>{Leave.createdAt}</h5>
      <h6 style={style}>From {Leave.from}</h6>
      <h6 style={style}>To {Leave.to}</h6>
      <div className="stamp ">
        <img className="img-thumbnail" src={img} alt="stuts" />
      </div>
    </div>
  );
};

export default SingleLeave;
