import React from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const FundExcel = ({ funds, name, element }) => {
  return (
    <ExcelFile element={element}>
      <ExcelSheet data={funds} name={name}>
        <ExcelColumn label="Fund Id" value="fundId" />
        <ExcelColumn label="Requested By" value="fundRequester" />
        <ExcelColumn label="Fund Items" value="fundItems" />
        <ExcelColumn label="Created At" value="fundDate" />
        <ExcelColumn label="Total Amount" value="fundAmount" />
        <ExcelColumn label="Check Number" value="checkNumber" />
        <ExcelColumn
          label="Approved"
          value={col => (col.approved ? "Approved" : "Rejected")}
        />
      </ExcelSheet>
    </ExcelFile>
  );
};

export default FundExcel;
