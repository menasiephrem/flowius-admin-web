import React, { Component } from "react";
//import _ from "lodash";
import BlockUi from "react-block-ui";
import Items from "./addItems";
import ClientSession from "../services/client-session";
import "react-block-ui/style.css";
import "loaders.css/loaders.min.css";
import Fund from "../services/fundServices";

class FundForm extends Component {
  state = {
    items: [],
    errors: [],
    disable: true,
    blocking: false
  };

  ITEM = { item: "", amount: 0 };
  ERROR = { item: "", amount: "" };

  addData = (index, data, error) => {
    const items = [...this.state.items];
    const errors = [...this.state.errors];
    items[index] = data;
    if (error) errors[index] = error;
    else errors[index] = this.ERROR;
    this.setState({ items, errors });
  };

  componentDidMount() {
    const items = this.state.items;
    const errors = this.state.errors;
    items.push(this.ITEM);
    errors.push(this.ERROR);
    this.setState({ items, errors });
  }

  handleAdd = () => {
    const items = [...this.state.items];
    const errors = [...this.state.errors];
    items.push(this.ITEM);
    errors.push(this.ERROR);

    this.setState({ items, errors });
  };

  handleRemove = index => {
    const items = this.state.items;
    const errors = this.state.errors;
    items.splice(index, 1);
    errors.splice(index, 1);
    this.setState({ items, errors });
  };

  noErrors = () => {
    let errors = [...this.state.errors];
    let numberOfErros = [];
    for (let i = 0; i < errors.length; i++) {
      let error = JSON.parse(JSON.stringify(errors[i]));
      if (!("amount" in error) && !("item" in error)) {
        numberOfErros.push(error);
      }
    }
    return errors.length === numberOfErros.length;
  };

  prepareData = () => {
    let fund = {};
    const now = new Date();
    const { username, email } = ClientSession.getAuth();
    fund.approved = false;
    fund.craterName = username;
    fund.createdAt = now;
    fund.createdBy = email;
    fund.fundId = `${now.getFullYear()}${("0" + (now.getMonth() + 1)).slice(
      -2
    )}.${Math.floor(1000 + Math.random() * 9000)}`;
    fund.items = [];
    this.state.items.map(item => {
      let Item = {};
      Item.item = item.item;
      Item.amount = Number(item.amount);
      return fund.items.push(Item);
    });
    fund.receiptURL = null;
    fund.reviewed = false;
    fund.supervisor = "chris@3blenterprises.com";
    fund.updatedAt = now;
    return fund;
  };

  handleSubmit = async () => {
    this.setState({ blocking: true });
    const fund = this.prepareData();
    await Fund.createFund(fund);
    this.setState({ blocking: false });
    this.props.history.push("/dashbord/funds");
  };

  render() {
    return (
      <BlockUi tag="div" blocking={this.state.blocking} message="Saving ...">
        <div>
          {this.state.items.map((item, index) => {
            return (
              <Items
                data={item}
                errors={this.state.errors[index]}
                index={index}
                showMore={index + 1 === this.state.items.length}
                removeble={index + 1 !== this.state.items.length}
                handleAdd={this.handleAdd}
                handleRemove={this.handleRemove}
                addData={this.addData}
              />
            );
          })}

          <button
            onClick={this.handleSubmit}
            disabled={!this.noErrors()}
            className="btn btn-primary"
          >
            Submit
          </button>
        </div>
      </BlockUi>
    );
  }
}

export default FundForm;
