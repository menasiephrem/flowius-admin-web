import React from "react";
import Fab from "@material-ui/core/Fab";
import Joi from "joi-browser";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Form from "./common/form";
import Input from "./common/input";

class Items extends Form {
  schema = {
    item: Joi.string()
      .required()
      .min(1)
      .label("Item Name"),
    amount: Joi.number()
      .required()
      .min(1)
      .label("Amount")
  };

  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.props.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    const data = { ...this.props.data };
    data[input.name] = input.value;
    const { index, addData } = this.props;
    addData(index, data, errors);
  };

  validate = () => {
    const opt = { abortEarly: false };
    const { error } = Joi.validate(this.props.data, this.schema, opt);
    if (!error) return null;

    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  renderInput(name, label, type = "text") {
    const { data, errors } = this.props;
    return (
      <Input
        type={type}
        name={name}
        value={data[name]}
        label={label}
        onChange={this.handleChange}
        error={errors ? errors[name] : ""}
      />
    );
  }

  render() {
    const { index, showMore, removeble, handleRemove, handleAdd } = this.props;
    return (
      <div className="row">
        <div className="col-3 m-2">{this.renderInput("item", "Item Name")}</div>
        <div className="col-3 m-2">
          {this.renderInput("amount", "Amount")}
          <div style={{ textAlign: "right" }} className="center-block">
            {showMore && (
              <Fab
                disabled={this.validate()}
                size="small"
                color="primary"
                aria-label="Add"
                onClick={handleAdd}
              >
                <AddIcon />
              </Fab>
            )}
          </div>
        </div>
        <div className="col-3 mt-3">
          {removeble && (
            <IconButton
              onClick={() => handleRemove(index)}
              aria-label="Delete"
              className="mt-2"
            >
              <DeleteIcon fontSize="small" />
            </IconButton>
          )}
        </div>
      </div>
    );
  }
}

export default Items;
