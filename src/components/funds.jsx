import React, { Component } from "react";
import _ from "lodash";
import ListGroup from "./common/listGroup";
import Search from "./common/search";
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";
import FundTable from "./fundsTabel";
import Loading from "./common/loading";
import Fund from "../services/fundServices";
import ClientSession from "../services/client-session";
import { toast } from "react-toastify";
import FirestoreDoc from "./common/firestoreDoc";
import LeaveAPI from "../services/leaveServices";
import FundDetailModal from "./modals/fund-detail";
import DatePicker from "./common/datePickerModal";

class Funds extends Component {
  state = {
    funds: [],
    catagories: [
      { name: "All Funds", _id: "allFunds" },
      { name: "Approved Funds", _id: true },
      { name: "Requested Funds", _id: false }
    ],
    pageSize: 8,
    currentPage: 1,
    sortColumn: { path: "fundId", order: "asc" },
    currentSelction: "All Funds",
    searchPhrase: "",
    isLoading: true,
    currentFund: "",
    modalOpen: false,
    nestedModal: false,
    checkNumber: null,
    reportModal: false,
    startDate: null,
    finishDate: null,
    reportDuration: null,
    reportFund: null,
    dateNumber: 0,
    errorOnNewFund: false
  };

  handleSearch = query => {
    this.setState({
      searchPhrase: query,
      currentPage: 1,
      currentSelction: null
    });
  };

  handleReviewChange = currentSelction => {
    this.setState({ currentSelction });
  };

  handlePageChange = currentPage => {
    this.setState({ currentPage });
  };

  async componentDidMount() {
    const funds = Fund.model(await Fund.getFund(ClientSession.getAuth().email));
    this.setState({ funds, isLoading: false });
  }

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handleDetail = id => {
    this.setState({ currentFund: id, modalOpen: true });
  };

  handleToggle = () => {
    this.setState({ modalOpen: false });
  };

  toggleReport = () => {
    this.setState({ reportModal: !this.state.reportModal });
  };

  handleApprove = async (id, status) => {
    toast("Updating Fund");
    await Fund.updateFund(id, { approved: status, reviewed: true });
    const funds = [...this.state.funds];
    const index = funds.findIndex(i => (i._id = id));
    funds[index].approved = status;
    funds[index].reviewed = true;
    this.setState({ funds, modalOpen: false });
  };

  toggleNested = () => {
    this.setState({ nestedModal: !this.state.nestedModal });
  };

  handleCheck = async id => {
    toast("Updating Fund");
    this.toggleNested();
    const checkNumber = this.state.checkNumber;
    await Fund.updateFund(id, { checkNumber });
  };

  updateCheck = e => {
    this.setState({ checkNumber: e.target.value });
  };

  handleDateRange = (startDate, endDate) => {
    const dateNumber = LeaveAPI.getDate(startDate.clone(), endDate.clone());

    this.setState({
      startDate: startDate,
      finishDate: endDate,
      dateNumber,
      reportFund: null
    });
  };

  handleSave = async () => {
    const { startDate, finishDate } = this.state;
    if (startDate && finishDate) {
      toast("Genrating Report");
      const funds = Fund.model(
        await Fund.getFundsInTime(startDate.toDate(), finishDate.toDate())
      );
      this.setState({ reportFund: funds });
    } else {
      toast.error("Please Select Start and Finsh Date");
    }
  };

  getpagedData = () => {
    const {
      pageSize,
      currentPage,
      funds,
      currentSelction,
      sortColumn,
      searchPhrase
    } = this.state;

    let filterdFunds = [...funds];
    if (searchPhrase !== "")
      filterdFunds = funds.filter(f =>
        f.fundId.toLowerCase().startsWith(searchPhrase.toLowerCase())
      );
    else if (currentSelction !== "All Funds")
      filterdFunds = funds.filter(
        fund => fund.reviewed === (currentSelction === "Approved Funds")
      );

    const sort = _.orderBy(filterdFunds, [sortColumn.path], [sortColumn.order]);
    const PagnatedFunds = paginate(sort, currentPage, pageSize);
    const count = filterdFunds.length;

    return { count, PagnatedFunds };
  };

  render() {
    const {
      catagories,
      currentSelction,
      searchPhrase,
      pageSize,
      sortColumn,
      currentPage,
      isLoading,
      modalOpen,
      currentFund,
      nestedModal,
      reportModal,
      startDate,
      finishDate,
      dateNumber
    } = this.state;
    const { count, PagnatedFunds: funds } = this.getpagedData();
    const isAdmin = ClientSession.getAuth().isAdmin;
    return (
      <React.Fragment>
        {modalOpen && (
          <FirestoreDoc path={`Funds/${currentFund}`}>
            <FundDetailModal
              isOpen={modalOpen}
              nestedModal={nestedModal}
              toggleNested={this.toggleNested}
              updateCheck={this.updateCheck}
              handleCheck={this.handleCheck}
              handleApprove={this.handleApprove}
              currentFund={currentFund}
              handleToggle={this.handleToggle}
              isAdmin={isAdmin}
            />
          </FirestoreDoc>
        )}

        <DatePicker
          isOpen={reportModal}
          headerText="New Fund Report"
          startDate={startDate}
          finishDate={finishDate}
          handleDateRange={this.handleDateRange}
          handleSave={this.handleSave}
          toggelCancel={this.toggleReport}
          dateNumber={dateNumber}
          reportFund={this.state.reportFund}
        />
        <div className="row">
          <div className="col-3 m-2">
            <ListGroup
              categories={catagories}
              selectedItem={currentSelction}
              onItemSelect={this.handleReviewChange}
            />
          </div>
          <div className="col-sm">
            <button
              onClick={() => this.props.history.push("/dashbord/funds/new")}
              className="btn btn-primary m-2"
            >
              New Fund
            </button>
            {isAdmin && (
              <button
                onClick={this.toggleReport}
                className="btn btn-primary m-2"
              >
                Genrate Report
              </button>
            )}
            <p className="mt-2">{`Showing ${count} Fund Requets in the databse`}</p>

            {isLoading ? (
              <Loading />
            ) : (
              <div>
                <Search value={searchPhrase} onChange={this.handleSearch} />

                <FundTable
                  onClick={this.handleDetail}
                  funds={funds}
                  sortColumn={sortColumn}
                  onSort={this.handleSort}
                />

                <Pagination
                  itemsCount={count}
                  pageSize={pageSize}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Funds;
