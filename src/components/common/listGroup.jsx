import React from "react";
const ListGroup = ({
  categories,
  textProperty,
  valueProperty,
  selectedItem,
  onItemSelect
}) => {
  const name = "list-group-item list-group-item-action ";

  return (
    <div className="list-group">
      {categories.map(category => {
        const active = category.name === selectedItem ? name + " active" : name;
        return (
          <li
            key={category[textProperty]}
            onClick={() => onItemSelect(category.name)}
            className={active}
          >
            {category[valueProperty]}
          </li>
        );
      })}
    </div>
  );
};

ListGroup.defaultProps = {
  textProperty: "_id",
  valueProperty: "name"
};

export default ListGroup;
