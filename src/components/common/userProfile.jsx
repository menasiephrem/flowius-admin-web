import React from "react";
import Avatar from "react-avatar";

const UserProfile = ({ url, name, handleClick }) => {
  return (
    <div>
      <Avatar
        round
        src={url}
        size="40"
        onClick={() => handleClick()}
        style={{ cursor: "pointer" }}
      />
      <span className="m-2">{name}</span>
    </div>
  );
};

export default UserProfile;
