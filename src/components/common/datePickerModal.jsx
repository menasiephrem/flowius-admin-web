import React, { Component } from "react";
import DateTimeRangeContainer from "react-advanced-datetimerange-picker";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import moment from "moment";
import FundExcel from "../fundExcel";

class DatePicker extends Component {
  state = {};

  render() {
    let now = new Date();
    let start = moment(
      new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0)
    );
    let end = moment(new Date(new Date().getFullYear() + 1, 0, 1, 0, 0, 0, 0));

    let local = {
      format: "DD-MM-YYYY HH:mm",
      sundayFirst: false
    };

    const {
      isOpen,
      headerText,
      startDate,
      finishDate,
      handleDateRange,
      handleSave,
      toggelCancel,
      dateNumber,
      reportFund
    } = this.props;
    return (
      <Modal isOpen={isOpen}>
        <ModalHeader>{headerText}</ModalHeader>
        <ModalBody>
          <div>
            <DateTimeRangeContainer
              start={start}
              end={end}
              local={local}
              applyCallback={handleDateRange}
            >
              <Button color="primary">Select Dates</Button>
            </DateTimeRangeContainer>
          </div>
          <p>
            {startDate ? `${startDate.format(" MMMM Do YYYY, h:mm a")} =>` : ""}
            {finishDate
              ? `${finishDate.format(
                  " MMMM Do YYYY, h:mm a"
                )}  (${dateNumber} Hours)`
              : ""}
          </p>
        </ModalBody>
        <ModalFooter>
          {reportFund && (
            <FundExcel
              funds={reportFund}
              element={
                <button color="primary" className="btn btn-primary mr-auto">
                  Download Genrated Report
                </button>
              }
              name="Fund Report"
            />
          )}
          <Button color="primary" onClick={handleSave}>
            Save
          </Button>
          <Button color="secondary" onClick={toggelCancel}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default DatePicker;
