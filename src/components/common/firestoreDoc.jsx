import React from "react";
import { FirestoreDocument } from "react-firestore";

import Loading from "./loading";

const FirestoreDoc = ({ path, children }) => {
  return (
    <FirestoreDocument
      path={path}
      render={({ isLoading, data }) => {
        return isLoading ? (
          <Loading modal />
        ) : (
          React.cloneElement(children, { data: data })
        );
      }}
    />
  );
};

export default FirestoreDoc;
