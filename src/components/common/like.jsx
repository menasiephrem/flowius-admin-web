import React from "react";
const Like = ({ handelFav, fav }) => {
  return (
    <div>
      <i
        onClick={() => handelFav()}
        className={fav ? "fa fa-heart" : "fa fa-heart-o"}
        aria-hidden="true"
        style={{ cursor: "pointer" }}
      />
    </div>
  );
};

export default Like;
