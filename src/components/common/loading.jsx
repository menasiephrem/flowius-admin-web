import React from "react";
import { css } from "@emotion/core";
import { DotLoader } from "react-spinners";
import { Modal, ModalBody } from "reactstrap";

const Loading = ({ modal }) => {
  return modal ? (
    <Modal isOpen={true}>
      <ModalBody>
        <div className="sweet-loading">
          <DotLoader
            css={css}
            sizeUnit={"px"}
            size={50}
            color={"#2ACEAD"}
            loading={true}
          />
        </div>
      </ModalBody>
    </Modal>
  ) : (
    <div className="sweet-loading">
      <DotLoader
        css={css}
        sizeUnit={"px"}
        size={50}
        color={"#2ACEAD"}
        loading={true}
      />
    </div>
  );
};

export default Loading;
