import React from "react";

const Apporved = ({ fav }) => {
  return (
    <div>
      <i
        className={fav ? "fa fa-check-circle" : "fa fa-ban"}
        aria-hidden="true"
      />
    </div>
  );
};

export default Apporved;
