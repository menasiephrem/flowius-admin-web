import React from "react";

const BootStrapInput = ({ label, onChange }) => {
  return (
    <div className="input-group">
      <div className="input-group-prepend">
        <span className="input-group-text" id="">
          {label}
        </span>
      </div>
      <input type="text" className="form-control" onChange={onChange} />
    </div>
  );
};

export default BootStrapInput;
