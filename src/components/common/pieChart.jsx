import React, { Component } from "react";
import PieChart from "react-minimal-pie-chart";

class Pie extends Component {
  render() {
    const { data, totalValue } = this.props;
    return (
      <PieChart
        data={data}
        totalValue={totalValue}
        lineWidth={20}
        label
        labelStyle={{
          fontSize: "25px",
          fontFamily: "sans-serif"
        }}
        labelPosition={0}
        animate
      />
    );
  }
}

export default Pie;
