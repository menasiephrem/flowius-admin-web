import React, { Component } from "react";
import Approved from "./common/approved";
import Table from "./common/table";

class FundTable extends Component {
  columns = [
    {
      path: "fundId",
      label: "Fund ID",
      content: fund => (
        <div
          style={{ cursor: "pointer", color: "#007bff" }}
          className="link"
          onClick={() => this.props.onClick(fund._id)}
        >
          {fund.fundId}
        </div>
      )
    },
    { path: "fundItems", label: "Fund Items" },
    { path: "fundAmount", label: "Fund Amount" },
    { path: "fundDate", label: "Fund Date" },
    { path: "fundRequester", label: "Requested By" },
    {
      label: "Approved",
      content: fund => <Approved fav={fund.approved} />
    }
  ];
  render() {
    const { funds, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={funds}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default FundTable;
