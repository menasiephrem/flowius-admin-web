import React, { Component } from "react";
import { Card } from "reactstrap";
import { auth } from "../services/index";
import Grid from "@material-ui/core/Grid";
import logoBlue from "../assets/flowius-admin-blue.png";
import GoogleButton from "react-google-button";
import { message } from "antd";
import "antd/dist/antd.css";

class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.authWithGoogle = this.authWithGoogle.bind(this);
    this.state = {};
  }

  state = {};

  authWithGoogle() {
    auth.doSignUpWithGoogle().then(
      response => {
        if (response.success) {
          message.success(response.message);
          setTimeout(() => {
            this.props.history.push("/dashbord/funds");
          }, 2000);
        } else {
          message.error(response.message);
        }
      },
      error => {
        message.error(error);
      }
    );
  }

  render() {
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ minHeight: "100vh" }}
      >
        <Grid item xs={3}>
          <Card className="box">
            <div className="logoContainer">
              <img className="logo" src={logoBlue} alt="flowius admin logo" />
            </div>

            <div className="signinButton">
              <GoogleButton
                onClick={() => {
                  this.authWithGoogle();
                }}
              />
            </div>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

export default Login;
