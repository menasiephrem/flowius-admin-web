import React, { Component } from "react";
import { Navbar, Nav, Dropdown } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import logo from "../assets/flowius-admin-blue.png";
import NavToggle from "./navToggle";
import ClientSession from "../services/client-session";

class NavBar extends Component {
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">
            <img
              src={logo}
              width="70"
              height="30"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <NavLink className="nav-item nav-link " to="/dashbord/funds">
                Fund
              </NavLink>
              <NavLink className="nav-item nav-link " to="/dashbord/leaves">
                Leave
              </NavLink>
              {ClientSession.getAuth().isAdmin && (
                <NavLink
                  className="nav-item nav-link "
                  to="/dashbord/employees"
                >
                  Employees
                </NavLink>
              )}
            </Nav>
            <Dropdown drop="left">
              <Dropdown.Toggle as={NavToggle} id="dropdown-custom-components">
                Text
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item href="/logout">Logout</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default NavBar;
