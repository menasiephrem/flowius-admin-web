import React, { Component } from "react";
import _ from "lodash";
import Search from "../common/search";
import Pagination from "../common/pagination";
import { paginate } from "../../utils/paginate";
import Loading from "../common/loading";
import EmployeeTable from "./employeeTabel";
import Employee from "../../services/employeeServices";

class Employees extends Component {
  state = {
    employees: [],
    pageSize: 8,
    currentPage: 1,
    sortColumn: { path: "name", order: "asc" },
    searchPhrase: "",
    isLoading: true
  };

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      employees,
      sortColumn,
      searchPhrase
    } = this.state;

    let filterdEmployees = [...employees];
    if (searchPhrase !== "")
      filterdEmployees = employees.filter(e =>
        e.name.toLowerCase().startsWith(searchPhrase.toLowerCase())
      );

    const sort = _.orderBy(
      filterdEmployees,
      [sortColumn.path],
      [sortColumn.order]
    );
    const PagnatedEmployees = paginate(sort, currentPage, pageSize);
    const count = filterdEmployees.length;

    return { count, PagnatedEmployees };
  };

  toggelNewEmployee = () => {};

  handleSearch = query => {
    this.setState({
      searchPhrase: query,
      currentPage: 1
    });
  };

  handlePageChange = currentPage => {
    this.setState({ currentPage });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  async componentDidMount() {
    const employees = Employee.model(await Employee.getEmployees());
    this.setState({ employees, isLoading: false });
  }

  render() {
    const {
      isLoading,
      searchPhrase,
      sortColumn,
      pageSize,
      currentPage
    } = this.state;
    const { count, PagnatedEmployees: employees } = this.getPagedData();

    return (
      <div className="col">
        <button
          onClick={this.toggelNewEmployee}
          className="btn btn-primary m-2"
        >
          New Employee
        </button>
        <p className="mt-2">{`Showing ${count} Employees in the databse`}</p>
        {isLoading ? (
          <Loading />
        ) : (
          <div>
            <Search value={searchPhrase} onChange={this.handleSearch} />
            <EmployeeTable
              onClick={this.handleDetail}
              employees={employees}
              sortColumn={sortColumn}
              onSort={this.handleSort}
            />

            <Pagination
              itemsCount={count}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
        )}
      </div>
    );
  }
}

export default Employees;
