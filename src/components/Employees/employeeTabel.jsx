import React, { Component } from "react";
import UserProfile from "../common/userProfile";
import Table from "../common/table";

class EmployeeTable extends Component {
  columns = [
    {
      label: "Name",
      path: "name",
      content: employee => (
        <UserProfile
          url={employee.picUrl}
          name={employee.name}
          handleClick={this.props.onClick}
        />
      )
    },
    { path: "email", label: "Email" },
    { path: "sickLeave", label: "Sick Leave Left" },
    { path: "annualLeave", label: "Annual Leave Left" },
    { path: "sickLeaveWDoctor", label: "Sick Leave With Doctors Note" }
    // {
    //   label: "Approved",
    //   content: leave => <Approved fav={leave.approved} />
    // }
  ];
  render() {
    const { employees, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={employees}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default EmployeeTable;
