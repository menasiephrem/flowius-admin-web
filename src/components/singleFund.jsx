import React from "react";
import approved from "../assets/approved.png";
import rejected from "../assets/rejected.png";
import pending from "../assets/pending.png";
import "./stylesheet.css";

const SingleFund = ({ Fund, Items }) => {
  const img = Fund.reviewed ? (Fund.approved ? approved : rejected) : pending;
  return (
    <div className="text-center font-weight-light">
      <h2>{Fund.fundRequester}</h2>
      <h2>{Fund.fundDate}</h2>
      <div className="p-4">
        {Items.map(item => {
          return (
            <h5>
              {item.item} -> {item.amount} Birr
            </h5>
          );
        })}
      </div>
      <h4>
        {Fund.fundItems} -> {Fund.fundAmount}
      </h4>
      <div className="stamp ">
        <img className="img-thumbnail" src={img} alt="stuts" />
      </div>
    </div>
  );
};

export default SingleFund;
