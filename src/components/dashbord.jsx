import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./navBar";
import Funds from "./funds";
import FundForm from "./addFund";
import PageNotFound from "./common/pageNotFound";
import ProtectedRoute from "./common/protectedRoute";
import Leave from "./leave";
import Employees from "./Employees/employees";
import ClientSession from "../services/client-session";

class Dashbord extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <main className="container m-4">
          <Switch>
            <Route path="/dashbord/funds/new" exact component={FundForm} />
            <Route path="/dashbord/funds" component={Funds} />
            <Route path="/dashbord/leaves" component={Leave} />
            <ProtectedRoute
              protection={ClientSession.getAuth().isAdmin}
              path="/dashbord/employees"
              component={Employees}
            />
            <Route path="/not-found" component={PageNotFound} />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default Dashbord;
