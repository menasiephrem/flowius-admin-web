import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import SingleFund from "../singleFund";
import Fund from "../../services/fundServices";
import BootStrapInput from "../common/bsInput";

const FundDetailModal = ({
  isOpen,
  nestedModal,
  data,
  toggleNested,
  updateCheck,
  handleCheck,
  handleApprove,
  currentFund,
  handleToggle,
  isAdmin
}) => {
  return (
    <Modal isOpen={isOpen}>
      <ModalHeader>
        <h4>{Fund.getFundModel(data).fundId}</h4>
        {Fund.getFundModel(data).checkNumber}
      </ModalHeader>
      <ModalBody>
        <SingleFund Items={data.items} Fund={Fund.getFundModel(data)} />

        <Modal isOpen={nestedModal} toggle={toggleNested}>
          <ModalHeader>Add Check Number</ModalHeader>
          <ModalBody>
            <BootStrapInput label="Check Number" onChange={updateCheck} />
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => handleCheck(currentFund)}>
              Done
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </ModalBody>
      <ModalFooter>
        {isAdmin && !data.reviewed && (
          <button
            color="primary"
            className="btn btn-danger mr-auto"
            onClick={() => handleApprove(currentFund, false)}
          >
            Decline Request
          </button>
        )}
        {isAdmin && !data.reviewed && (
          <Button
            color="primary"
            onClick={() => handleApprove(currentFund, true)}
          >
            Apporve Request
          </Button>
        )}
        {isAdmin && data.approved && (
          <Button color="primary" onClick={toggleNested}>
            Add Check Number
          </Button>
        )}
        <Button color="secondary" onClick={handleToggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default FundDetailModal;
