import React, { Component } from "react";
import Approved from "./common/approved";
import Table from "./common/table";

class LeaveTable extends Component {
  columns = [
    {
      path: "leaveId",
      label: "Leave ID",
      content: leave => (
        <div
          style={{ cursor: "pointer", color: "#007bff" }}
          className="link"
          onClick={() => this.props.onClick(leave.leaveId)}
        >
          {leave.leaveId}
        </div>
      )
    },
    { path: "duration", label: "Duration" },
    { path: "requestedAmount", label: "Requested Amount" },
    { path: "createdAt", label: "Created At" },
    { path: "leaveRequester", label: "Requested By" },
    {
      label: "Approved",
      content: leave => <Approved fav={leave.approved} />
    }
  ];
  render() {
    const { leaves, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={leaves}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default LeaveTable;
