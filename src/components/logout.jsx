import { Component } from "react";
import ClientSession from "../services/client-session";

class Logout extends Component {
  componentDidMount() {
    ClientSession.Logout();
    window.location = "/";
  }
  render() {
    return null;
  }
}

export default Logout;
