import React, { Component } from "react";
import Avatar from "react-avatar";
import ClientSession from "../services/client-session";

class NavToggle extends Component {
  handleClick = e => {
    e.preventDefault();

    this.props.onClick(e);
  };
  render() {
    return (
      <Avatar
        round
        src={ClientSession.getAuth().photoURL}
        size="40"
        onClick={this.handleClick}
        style={{ cursor: "pointer" }}
      />
    );
  }
}

export default NavToggle;
